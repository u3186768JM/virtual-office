package com.example.virtualoffice;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.virtualoffice.Adapter.ToDoAdapter;
import com.example.virtualoffice.Model.ToDoModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link task#newInstance} factory method to
 * create an instance of this fragment.
 */
public class task extends Fragment {

    private RecyclerView tasksRecyclerView;
    private ToDoAdapter tasksAdapter;

    private List<ToDoModel> taskList;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public task() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment task.
     */
    // TODO: Rename and change types and number of parameters
    public static task newInstance(String param1, String param2) {
        task fragment = new task();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // getSupportActionBar().hide();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

            //
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
       View view = inflater.inflate(R.layout.fragment_task, container, false);

       taskList = new ArrayList<>();
        tasksRecyclerView = view.findViewById(R.id.tasksRecyclerView);
        RecyclerView.LayoutManager task_layout = (new LinearLayoutManager(getContext()));
        tasksRecyclerView.setLayoutManager(task_layout);
        tasksAdapter = new ToDoAdapter(this);
        tasksRecyclerView.setAdapter(tasksAdapter);

        ToDoModel task1 = new ToDoModel();
        task1.setTask("Finish developing prototype");
        task1.setStatus(0);
        task1.setId(1);
        task1.setDate("Tomorrow");

        ToDoModel task2 = new ToDoModel();
        task2.setTask("Email client about billing");
        task2.setStatus(1);
        task2.setId(1);
        task2.setDate("In one week");

        ToDoModel task3 = new ToDoModel();
        task3.setTask("Reschedule meetings");
        task3.setStatus(0);
        task3.setId(1);
        task3.setDate("20/05/2021");

        ToDoModel task4 = new ToDoModel();
        task4.setTask("Look over report");
        task4.setStatus(0);
        task4.setId(1);
        task4.setDate("21/05/2021");

        ToDoModel task5 = new ToDoModel();
        task5.setTask("Tidy up desk");
        task5.setStatus(0);
        task5.setId(1);
        task5.setDate("No due date");

        taskList.add(task1);
        taskList.add(task2);
        taskList.add(task3);
        taskList.add(task4);
        taskList.add(task5);

        tasksAdapter.setTasks(taskList);



        return view;
    }

//    public void addNewTask(View view){
//        Intent intent = new Intent (this, );
//        startActivity(intent);
//    }
}