package com.example.virtualoffice;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.virtualoffice.Adapter.ToDoAdapter;
import com.example.virtualoffice.Model.ToDoModel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link calendar#newInstance} factory method to
 * create an instance of this fragment.
 */
public class calendar extends Fragment {


    private RecyclerView calendarRecyclerView;
    private ToDoAdapter calendarAdapter;

    private List<ToDoModel> calendarList;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public calendar() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment calendar.
     */
    // TODO: Rename and change types and number of parameters
    public static calendar newInstance(String param1, String param2) {
        calendar fragment = new calendar();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_calendar, container, false);


        calendarList = new ArrayList<>();
        calendarRecyclerView = view.findViewById(R.id.calendarRecyclerView);
        RecyclerView.LayoutManager task_layout = (new LinearLayoutManager(getContext()));
        calendarRecyclerView.setLayoutManager(task_layout);
        calendarAdapter = new ToDoAdapter(this);
        calendarRecyclerView.setAdapter(calendarAdapter);

        ToDoModel task = new ToDoModel();
        task.setTask("Meeting with Jenny");
        task.setStatus(0);
        task.setId(1);
        task.setDate("10:00 AM");

        ToDoModel task2 = new ToDoModel();
        task2.setTask("Zoom call with Customer");
        task2.setStatus(0);
        task2.setId(1);
        task2.setDate("1:00 PM");

        calendarList.add(task);
        calendarList.add(task2);


        calendarAdapter.setTasks(calendarList);
        return view;
    }


}