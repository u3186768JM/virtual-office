package com.example.virtualoffice.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.virtualoffice.R;
import com.example.virtualoffice.calendar;
import com.example.virtualoffice.task;
import com.example.virtualoffice.Model.ToDoModel;

import java.util.List;

public class ToDoAdapter extends RecyclerView.Adapter<ToDoAdapter.ViewHolder> {
    private List<ToDoModel> todoList;
    private task activity;
    private task calendar;

    public ToDoAdapter(task activity){
        this.activity = activity;
    }

    public ToDoAdapter(calendar activity){
        this.calendar = calendar;
    }


    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.task_layout, parent, false);
        return new ViewHolder(itemView);
    }

    public void onBindViewHolder(ViewHolder holder, int position){
        ToDoModel item = todoList.get(position);
        holder.task.setText(item.getTask());
        holder.task.setChecked(toBoolean(item.getStatus()));
        holder.todoDate.setText(item.getDate());
    }

    public int getItemCount(){
        return todoList.size();
    }

    private boolean toBoolean(int n){
        return n!=0;
    }

    public void setTasks(List<ToDoModel> todoList){
        this.todoList = todoList;
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{
        CheckBox task;
        TextView todoDate;

        ViewHolder(View view){
            super(view);
            task = view.findViewById(R.id.todoCheckBox);
            todoDate = view.findViewById(R.id.todoDate);
        }
    }
}
