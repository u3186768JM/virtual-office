package com.example.virtualoffice.ui.home;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CallLog;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.example.virtualoffice.MainActivity;
import com.example.virtualoffice.R;

import org.w3c.dom.Text;

import java.util.Date;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        TextView tv = (TextView) root.findViewById(R.id.phone_number);
        final String details = getCallDetails("David-J 2 05-04-21 12:35pm 34:23");
        final String details_2 = getCallDetails("(02)635678 1 07-04-21 1:35pm 30:22");
        final String details_3 = getCallDetails("Victoria-S 3 08-04-21 2:35pm 34:22");
        if (!details.isEmpty()) {
            StringBuffer stringBuffer = new StringBuffer();
            stringBuffer = new StringBuffer(details);

            tv.setText(stringBuffer+"\n");

            stringBuffer = new StringBuffer(details_2);
            tv.append(stringBuffer+"\n");
            stringBuffer = new StringBuffer(details_3);
            tv.append(stringBuffer);
        }
        tv.setMovementMethod(new ScrollingMovementMethod());
        return root;
    }
    private static String getCallDetails(String context) {
        StringBuffer stringBuffer = new StringBuffer();
        String[] splitString = new String[5] ;
        splitString = context.split(" ");
        String phNumber = splitString[0];
        String callType = splitString[1];
        String callDate = splitString[2];
        String callDayTime = splitString[3];
        String callDuration = splitString[4];
        String dir = null;
        int dircode = Integer.parseInt(callType);
        switch (dircode) {
            case CallLog.Calls.OUTGOING_TYPE: //value 2
                dir = "OUTGOING";
                break;

            case CallLog.Calls.INCOMING_TYPE: //value 1
                dir = "INCOMING";
                break;

            case CallLog.Calls.MISSED_TYPE: // value 3
                dir = "MISSED";
                break;
        }
        stringBuffer.append(phNumber + " \n"
                + dir + " \n" + callDayTime
                + " \n " + callDuration);
        stringBuffer.append("\n----------------------");
        return stringBuffer.toString();
    }
}
